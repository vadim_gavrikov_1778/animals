package ru.gva.animal;

/**
 * Класс расширяет абстрактный класс Animal.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Cat extends Animal {
    protected Cat(String name, String voice) {
        super(name, voice);
    }
    public Cat() {
        this("Кот","мур-мур");
    }
}
