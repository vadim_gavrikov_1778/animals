package ru.gva.animal;

/**
 * Класс расширяет абстрактный класс Animal.
 *
 * @author Gavrikov V.A. 15OIT18.
 */
public class Dog extends Animal {


    public Dog() {
        this("Собака", "гав-гав");
    }


    protected Dog(String name, String voice) {
        super(name, voice);
    }

}
