package ru.gva.animal;

/**
 * Класс расширяет абстрактный класс Animal.
 *
 * @author Gavrikov V.A. Group 15OIT18
 */
public class Cow extends Animal {


    protected Cow(String name, String voice){
        super(name, voice);
    }


    public Cow() {
        this("Корова","Му-му");
    }
}
