package ru.gva.animal;
/**
 * Тестовый класс.
 *
 * @author Gavrikov V.A. 15OIT18.
 */
public class Main {

    public static void main(String... args) {
        Dog dog = new Dog();
        Dog dog1 = new Dog("Собака", "гав-гав");
        Dog dog2 = new Dog("Тузик", "тяф-тяф");
        Cat cat = new Cat();
        Cat cat1 = new Cat("мурзик","Мур-мур");
        Cow cow = new Cow();
        Cow cow1 = new Cow("буренка","мууууу");

        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();
        cat.printDisplay();
        cat1.printDisplay();
        cow.printDisplay();
        cow1.printDisplay();
    }
}
